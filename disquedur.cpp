/*
    NOM: Arthson Mukeya
    NI : A00158681
    DEVOIR 1
*/
#include "disquedur.hpp"

/*
 *  @name  : DisqueDur
 *  @param : void
 *  @return: void
 *  @desc  : initialisation du csteur
*/
DisqueDur::DisqueDur(void)
{
    m_tpsRotation = 10;
    m_tpsSecteurPiste = 2;
    m_adressePrecedente = 0;
    m_tempsTotal = 0;
    m_access = false;

    int cpteur = 0;         //compteur pour initialiser les secteurs et les pistes
    for(int i(0); i < PISTE; i++)
    {
        for(int j(0); j < SECTEUR; j++)
        {
            m_plateau[i][j] = cpteur++ ;
        }
    }
}
/*
 *  @name  : ~DisqueDur
 *  @param : void
 *  @return: void
 *  @desc  : initialisation du destructeur
*/
DisqueDur::~DisqueDur(void){}
/*
 *  @name  :
 *  @param :
 *  @return:
 *  @desc  :
*/
void DisqueDur::display()
{
    for(int i(0); i < PISTE; i++)
    {
        for(int j(0); j < SECTEUR; j++)
        {
            cout << "[ " << this->m_plateau[i][j] << " ]\t";
        }
        cout << endl;
    }
}
/*
 *  @name  : getPiste
 *  @param : short file : adresse du fichier
 *  @return: short
 *  @desc  : renvoit le numéro de piste d'un fichier
*/
short DisqueDur::getPiste(short file)
{
    return file / SECTEUR;
}
/*
 *  @name  : getSecteur
 *  @param : short file : adresse du fichier
 *  @return: short
 *  @desc  : renvoit le numéro du secteur d'un fichier
*/
short DisqueDur::getSecteur(short file)
{
    return file % SECTEUR;
}
/*
 *  @name  : estAdjacent
 *  @param : fichier précédent(pFile) et fichier courant(cFile)
 *  @return: vrai ou faux
 *  @desc  : verifie si 2 pistes sont adjacentes ou 2 secteurs
*/
bool DisqueDur::estAdjacent(short pFile, short cFile)
{
    //on vérifie d'abord s'il sont sur une meme piste
    int piste_1 = this->getPiste(pFile);
    int piste_2 = this->getPiste(cFile);
    
    return  abs(piste_1 - piste_2) == 1;
}
/*
 *  @name  : estSurMemePiste
 *  @param : fichier précédent(pFile) et fichier courant(cFile)
 *  @return: vrai ou faux
 *  @desc  : verifie si 2 fichiers sont sur une meme piste
*/
bool  DisqueDur::estSurMemePiste(short pFile, short cFile)
{
    //on vérifie d'abord s'il sont sur une meme piste
    int piste_1 = this->getPiste(pFile);
    int piste_2 = this->getPiste(cFile);

    return piste_1 == piste_2;
}
/*
 *  @name  : getAdressePrecedente
 *  @param : short : fichier précédent
 *  @return: short : numero ou adresse du fichier
 *  @desc  : retourne l'adresse du fichier précédent
*/
short DisqueDur::getAdressePrecedente()
{
    return this->m_adressePrecedente ;
}
/*
 *  @name  : setAdressePrecedente
 *  @param : short : fichier précédent
 *  @return: void
 *  @desc  : calcul l'adresse du fichier précédent
*/
void  DisqueDur::setAdressePrecedente(short adresse)
{
    this->m_adressePrecedente = adresse;
}
/*
 *  @name  : readFile
 *  @param :
 *  @return:
 *  @desc  : calcule le temps que ça prend pour lire un fichier directement sur le disque
 *           tient compte des pistes adjacentes, et des fichier sur une meme piste
*/
void DisqueDur::readFile(short file)
{
    this->calculTempsAcces(file);
}
//sur le disque dur, les opérations ont les mêmes couts
//on utilisera les memes calculs de temps pour les autres opérations
void  DisqueDur::writeFile(short file)
{
    this->calculTempsAcces(file);
}
void  DisqueDur::modifyFile(short file)
{
    this->calculTempsAcces(file);
}
/*
 *  @name  : resetTempsTotal
 *  @param : void
 *  @return: void
 *  @desc  : utiliser pour remettre le compteur à zero quand on change de programme
*/
void DisqueDur::resetTempsTotal()
{
    this->m_tempsTotal = 0;
}
/*
 *  @name  : getTempsTotal
 *  @param : void
 *  @return: temps total en millisecondes
 *  @desc  : retourne le temps total pour les acess au disque dur
*/
int DisqueDur::getTempsTotal()
{
    return this->m_tempsTotal;
}
/*
 *  @name  : calcul temps acces
 *  @param : adresse du fichier : file
 *  @return: void(rien)
 *  @desc  : calculer le temps d'access à une données sur le disque dur
*/
void DisqueDur::calculTempsAcces(short file)
{
    if(this->m_access)
    {
        // access au disque plus d'une fois
        short pFile = this->m_adressePrecedente;
        short cFile = file;
        //on vérifie si les deux fichiers sont sur une meme piste
        if(this->estSurMemePiste(pFile, cFile)) // +2 ms
        {
            this->m_tempsTotal += this->m_tpsSecteurPiste;
        }
        else    // piste differentes
        {
            if(this->estAdjacent(pFile, cFile)) // +2ms+2ms
            {
                this->m_tempsTotal += this->m_tpsSecteurPiste * 2;
            }
            else // 10 ms + 2 ms
            {
                this->m_tempsTotal += this->m_tpsRotation + this->m_tpsSecteurPiste; 
            }
        }
        this->m_adressePrecedente = file; // à la fin des calculs, l'adresse present devient l'adresse précédente
    }
    else  // access au premier element pour la premiere fois 10 ms + 2ms
    {
        this->m_tempsTotal += this->m_tpsRotation + this->m_tpsSecteurPiste;
        this->m_access = true;
        this->m_adressePrecedente = file;
    }
}
// temps en milliseconde
// ajoute au temps total x ms
void DisqueDur::addToTemps(short ms)
{
    this->m_tempsTotal += ms;
}
/*
 *  @name  : dejaAcceder
 *  @param : void
 *  @return: vrai si le disque à déja été accéder ou faux si c'est la première fois
 *  @desc  : renvoit le compteur qui dit si le disque est déjà accedér ou pas
*/
bool DisqueDur::dejaAcceder()
{
    return this->m_access;
}
/*
 *  @name  : marquerAcces
 *  @param : bool acces
 *  @return: void
 *  @desc  : marque à vrai ou faux si l'access au disque es fait
*/
void  DisqueDur::marquerAcces(bool acces)
{
    this->m_access = acces;
}