/*
    NOM: Arthson Mukeya
    NI : A00158681
    DEVOIR 1
*/

#ifndef DISQUEDURE_H
#define DISQUEDURE_H

#define SECTEUR 5
#define PISTE   4

#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

class DisqueDur
{
public:
    DisqueDur(void);                    //csteur
    virtual ~DisqueDur(void);           //dsteur
    void  display(void);                 // afficher le contenu du disque dure (ici les fichiers)
    
    short getAdressePrecedente();           // va tracer l'element précédent
    void  setAdressePrecedente(short);
    void  calculTempsAcces(short);
    int   getTempsTotal(void);
    void  resetTempsTotal(void);
    void  readFile(short);              // lecture du fichier directement sur le disque dure
    void  writeFile(short);             // Ecriture du fichier ||
    void  modifyFile(short);            // Modification du fichier
    bool  dejaAcceder();
    void  marquerAcces(bool);
    void  addToTemps(short);            // ajoute un temps supplémentaire au temps total
private:
    short getPiste(short);              // renvoit la piste en fonction de l'adresse
    short getSecteur(short);            //renvoit toute les pistes d'un secteur
    bool  estAdjacent(short, short);    // verifie si 2 pistes sont adjacentes ou 2 secteurs
    bool  estSurMemePiste(short, short);// determine si les deux fichiers sont sur une meme piste
    
    short m_plateau[PISTE][SECTEUR];
    short m_tpsRotation;                //temps de rotation
    short m_tpsSecteurPiste;            //temps de lecture pour un secteur et une piste adjacente
    short m_adressePrecedente;
    int   m_tempsTotal;
    bool  m_access;
};

#endif