/*
    NOM: Arthson Mukeya
    NI : A00158681
    DEVOIR 1
*/
#include <iostream>
#include <string>
#include <stdexcept>
#include <iomanip>

#include "disquedur.hpp"
#include "tampon.hpp"

using namespace std;

short choix_programme(short);
short choix_heuristique(short);
double pourcentage_efficacite(int);

struct instructions_t
{
    short mod  = 0; //0 = lecture et 1 =  ecriture et 2 = modify
    short file = 0;
};

int main()
{
    srand( time( NULL ) );

    const int nbreProgrammes = 3;
    const int nbreFichiers   = 10000;
    cout << "Nom: " << "Arthson Mukeya" << endl;
    cout << "NI : " << "A00158681" << endl;
    cout << "Simulation du disque dur et du tampon " << endl;
    cout << endl;

    DisqueDur *hd = new DisqueDur();
    Tampon *tampon = new Tampon(5); // taille 5 heuristique 0(fifo)
    //on défini les programmes(3 prgrammes avec 10 fichiers)
    instructions_t programme[nbreProgrammes][nbreFichiers];
    //on initialise les programmes avec les fichier du disques dure
    for(int i(0); i < nbreProgrammes; i ++)
    {
        for(int j(0); j < nbreFichiers; j++)
        {
            instructions_t inst; //on choisit le mod du fichier soit r = 0 (lecture), w = 1 (ecriture), file(0-19) fichier sur disque dur
            inst.mod  = rand() % 3; // 0 = read, 1 = modify, 2 = write
            inst.file = rand() % 20;// 0-19 fichier sur disque
            programme[i][j] = inst;
        }
    }
    cout << endl;
    string choix   = "";    // contiendra q ou c pour continuer ou arreter le programme
    do
    {
        short h = choix_heuristique(2);                   // 2 représente les nombres d'heuristique disponible
        short n = choix_programme(nbreProgrammes);              //on demande à l'utilisateur, le programme à utiliser en fonction du nombre des programmes
        
        tampon->setHrstq(h);

        if(h == 0)
            cout << "Heuristique 1: File(FIFO)" << endl;
        else
            cout << "Heuristique 2: Moins souvent utilise(LRU)" << endl;

        cout << "debut du programmme..." << endl;
        cout << "programme à executer... : "<< (n+1) << endl;
        cout << "etat du bassin(derniers elements) ... : ";
        tampon->showBuffer();

        for(int i(0); i < nbreFichiers; i++)
        {
            instructions_t inst;
            inst.mod  = programme[n][i].mod;
            inst.file = programme[n][i].file;

            if(inst.mod == 0) // fichier en mode lecture
            {
                if(!tampon->fileInBuffer(inst.file)){
                    tampon->readFile(inst.file);    // 
                }
            }
            else if(inst.mod == 1) // fichier mode modify
            {
                if(!tampon->fileInBuffer(inst.file)){
                    tampon->modifyFile(inst.file);
                }
                else{
                    tampon->setFlagOnblock(inst.file);
                }
            }
            else{ // fichier en mode ecriture; s'il est dans le buffer on le flag
                if(tampon->fileInBuffer(inst.file)){
                    tampon->setFlagOnblock(inst.file);
                }
            }
        }
        unsigned int t_1(0), t_2(0), t_3(0); // 1 temps avec tampon avant de vider le tampon, 2 apres vidage tampon, 3 sans tampon
        cout << "execution du programme ..." << endl;
        cout << "fin du programme ... : " << (n+1) << endl;
        cout << "etat du bassin ... : ";
        tampon->showBuffer();
        t_1 = tampon->getTempsTotal();
        cout << "on vide le bassin .. " << endl;
        tampon->closeBuffer();

        t_2 = tampon->getTempsTotal();
        tampon->resetTempsTotal();
        cout << "Execution du programme sans utilisation du tampon" << endl;
        // les access directes au disque dure, consomme le meme temps
        for(int i(0); i < nbreFichiers; i++)
        {
            instructions_t inst;
            inst.mod  = programme[n][i].mod;
            inst.file = programme[n][i].file;

            if(inst.mod == 0){// fichier en mode lecture
                hd->readFile(inst.file);
            }
            else if(inst.mod == 1){// fichier mode modify
                hd->modifyFile(inst.file);
            }
            else{ // fichier en mode ecriture
                hd->writeFile(inst.file);
            }       
        }
        cout << "execution du programme ..." << endl;
        cout << "fin du programme ... : " << (n+1) << endl << endl;
        t_3 = hd->getTempsTotal();

        cout << "temps ecoule avant vidange tampon(1) : " << t_1 << " ms" << endl;
        cout << "temps ecoule apres vidange tampon(2) : " << t_2 << " ms" << endl;
        cout << "temps ecoule sans tampon(3)          : " << t_3 << " ms" << endl << endl;
        cout << "pourcentage d'efficacite gagne 2 & 3 : " 
        << fixed << setprecision(2) << pourcentage_efficacite(t_3 - t_1) << " %" << endl;

        // on réinitialise tous
        hd->setAdressePrecedente(0);
        hd->marquerAcces(false);
        hd->resetTempsTotal();
        //
        cout << "Appuyez sur (q) pour quitter ou (c) ou n'importe quelle touche pour continuer ..." << endl;
        cout << " > "; cin >> choix;
        if(choix == "q" || choix == "Q") break;
    }while(true);

    delete hd;
    delete tampon;

    return 0;
}
// fonction qui va renvoyer le choix du programme entré par le user
short choix_programme(short nbreProgrammes)
{
    string choix            = "";
    unsigned short  n       = 0;               // va contenir la variable entiere du choix
    bool   choix_incorrecte = true;
    do
    {
        cout << "Choisir le programme a executer (1-" << nbreProgrammes << "): "<< endl;
        cout << ">: "; cin >> choix;
        try{
            n = stoi(choix);
        }
        catch(exception &e){
            cerr << "chiffre invalide" << endl;
        }
        if(n <= nbreProgrammes && n > 0)
            choix_incorrecte = false;       //veut dire choix est correcte
        else
            choix_incorrecte = true;
    }while(choix_incorrecte);               // on redemandera à l'utilisateur d'entre un choix correcte
    
    return --n;                             // indice tableaux 0 - 2
}
// va renvoyer le choix de l'heuristique entré par le user
// n : nombre d'heuristique  disponible
short choix_heuristique(short n)
{
    unsigned short h(0);
    string _h = "";
    do
    {
        cout << "1: FIFO (Premier entre, premier sorti)" << endl;
        cout << "2: LRU  (Info la moins utilisee)" << endl;
        cout << "Selectionner l'heuristique : ";cin >> _h;
        try{
            h = stoi(_h);
        }
        catch(exception &e){
            cerr << "chiffre invalide" << endl;
        }
        if(h <= 0)
            cout << "Nombre > 0 " << endl;
        else if(h > n)
            cout << "Seulement deux heuristiques dispo" << endl;
        else
            break;
    }while(true);
    return --h;       // on retourne l'heuristique choisit
}
// methode pour calculer % d*efficacité
double pourcentage_efficacite(int n)
{
    double _n = (double) n;
    while(_n > 100) 
        _n /=100;
    return _n;
}