/*
    NOM: Arthson Mukeya
    NI : A00158681
    DEVOIR 1
*/

#ifndef TAMPON_H
#define TAMPON_H

#include <iostream>
#include <cstdlib>
#include <queue>
#include <list>
#include "disquedur.hpp"

using namespace std;

struct fichier_t
{
    int     elt;    //fichier du disque dure
    bool    flag;   //pour marquer le bit
    short   cpteur; //garde la trace du nombre de fois que le fichier est utilisé
    bool    operator<(fichier_t const &pt_elt)
    { return this->cpteur < pt_elt.cpteur ;} // permet de trier les fichier
};

class Tampon
{
public:
    Tampon(void);                   // heuristique 0 par défaut
    Tampon(short, short);           // specifie la taille et l'heuristique 0 = heuristique 1((FIFO)) et 1 heuristique 2(LRU)
    Tampon(short);                  // prend la taille seulement
    virtual ~Tampon(void);

    void    readFile(short);        // si fichier r dans le programme copie le fichier sur la cache
    void    writeFile(short);       // si fichier dans buffer, set flag to dirty, si pas dans buffer, on fait rien sur le disque
    void    modifyFile(short);
    void    closeFile(short);       // force l'ecriture du fichier sur le disque dure
    void    closeBuffer(void);      // vider le buffer en écrivant sur le disque tous les fichier avec un bit sale
    bool    fileInBuffer(short);    // verifie si le fichier est dans le buffer
    void    showBuffer(void);       // on affiche le buffer en fonction de l'heuristique donnée
    int     getTempsTotal(void);    // renvoit temps total
    void    resetTempsTotal(void);  // reset le temps à zéro
    short   getHrstq(void);         // va nous renvoyer l'heuristique à utiliser pour notre buffer
    void    setHrstq(short);        // va déterminer l'heuristique à utiliser
    void    setFlagOnblock(short);  // met le flag à true ou false
    bool    isFull(void);           // check if the buffer is full or not
    bool    isEmpty(void);          // check if buffer is empty or not
    short   getTailleTampon();      // renvoit la taille du tampon

private:
    short           m_hrstq;         // va définir quel heuristique utiliser.
    short           m_taille;        // taille du tampon
    list<fichier_t> m_tampon;        // sera notre tempon pour l'heuristique 0 ou par défaut
    list<fichier_t>::iterator it;
    short           m_cpteur;       // garde la trace de combien de fois l'écriture se fait sur le disque
    DisqueDur       *m_adress;       // une référence au disque dur lors de l'opération d'écriture
    void            buffer();        // fonction interne qui affiche simplement la liste sans heuristique
};

#endif