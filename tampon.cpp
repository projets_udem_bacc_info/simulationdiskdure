/*
    NOM: Arthson Mukeya
    NI : A00158681
    DEVOIR 1
*/
#include "tampon.hpp"

/*
 *  @name  : Tampon
 *  @param : void
 *  @return: void
 *  @desc  : initialisation du csteur
*/
Tampon::Tampon(void)
{
    this->m_taille  = 0;
    this->m_hrstq   = 0;
    this->m_tampon  = {};
    this->m_adress = new DisqueDur();
}
/*
 *  @name  : Tampon
 *  @param : taille du tampon
 *  @return: void
 *  @desc  : initialisation du csteur, renvoit une erreur si la taille est incorreste
*/
Tampon::Tampon(short taille)
{
    if(taille < 1)
    {
        cout << "Erreur, Taille Tampon : >= 1 " << endl;
        exit(EXIT_FAILURE);
    }
    this->m_taille  = taille;
    this->m_hrstq   = 0;
    this->m_tampon  = {};
    this->m_adress = new DisqueDur();
}
/*
 *  @name  : Tampon
 *  @param : taille du tampon et numéro correspondant à l'heuristique
 *  @return: void
 *  @desc  : initialisation du csteur
*/
Tampon::Tampon(short taille, short hrstq)
{
    if(taille < 1)
    {
        cout << "Erreur, Taille Tampon : >= 1 " << endl;
        exit(EXIT_FAILURE);
    }
    if(hrstq < 0)
    {
        cout << "Erreur, Heuristique : >= 0 " << endl;
        exit(EXIT_FAILURE);
    }
    //taille et heuristique correcte
    this->m_taille  = taille;
    this->m_hrstq   = hrstq;
    this->m_tampon  = {};
    this->m_cpteur  = 0;
    this->m_adress = new DisqueDur();    
}
/*
 *  @name  : ~Tampon
 *  @param : void
 *  @return: void
 *  @desc  : initialisation du destructeur
*/
Tampon::~Tampon(void){ delete this->m_adress;}
/*
 *  @name  : getTailleTampon
 *  @param : void
 *  @return: short : taille max du tampon
 *  @desc  : renvoit la taille max de notre tampon
*/
short Tampon::getTailleTampon() { return this-> m_taille; }
/*
 *  @name  : isEmpty
 *  @param : void
 *  @return: true si le tampon est vide
 *  @desc  : determine si le buffer est vide ou pas
*/
bool Tampon::isEmpty() { return this->m_tampon.empty();}
/*
 *  @name  : isFull
 *  @param : void
 *  @return: vrai si le tampon est rempli
 *  @desc  : vérifie la capacité du tampon
*/
bool Tampon::isFull()
{
    return this->getTailleTampon() <= this->m_tampon.size();
}
/*
 *  @name  : getHrstq
 *  @param : void
 *  @return: short : le numéro correspondant à l'heuristique utilisée
 *  @desc  : retourne l'heuristique courante  utilisée pour l'utilisation du tampon
*/
short Tampon::getHrstq() { return this->m_hrstq; }
/*
 *  @name  : setHrstq
 *  @param : short : le numéro correspondant à l'heuristique
 *  @return: void
 *  @desc  : permet de changer d'heuristique pour la gestion du tampon
*/
void Tampon::setHrstq(short hrstq) { this->m_hrstq = hrstq; }
/*
 *  @name  : setFlagOnblock
 *  @param : file : adresse du fichier
 *  @return: void
 *  @desc  : met le bit à dirty sur block ou se trouve l'adresse du fichier recu en paramètre
*/
void Tampon::setFlagOnblock( short file ) 
{
    for(it = this->m_tampon.begin();
        it != this->m_tampon.end(); it++)
    {
        if((*it).elt == file)
        {
            (*it).flag = true;
            (*it).cpteur++;
        }
    }
}
/*
 *  @name  : fileInBuffer
 *  @param : adresse ou numéro du fichier
 *  @return: vrai si fichier trouvé dans le fichier | non sinon
 *  @desc  : verifie si le fichier se trouve dans le buffer
*/
bool Tampon::fileInBuffer(short file)
{
    if(!this->m_tampon.empty())
    {
        for(it = this->m_tampon.begin();
        it != this->m_tampon.end(); it++) //on cherche le fichier
        {
            if((*it).elt == file) 
                return true;
        }
        return false;                     // fichier non trouvé
    }
    else{                                 //tampon vide
        return false;
    }
}
/*
 *  @name  : readFile
 *  @param : file : adresse du fichier sur disque dure
 *  @return: void
 *  @desc  : Va lire le fichier dans le tampon.
 *           Si le tampon es plein, il sera inséré selon l'heuristique souhaité.
*/
void Tampon::readFile(short file)
{
    // on complete le block du fichier
    fichier_t fichier;
    fichier.elt  = file;
    fichier.flag = 0;

    if(!this->isFull())                             //on vérifie si le tampon n'est pas encore plein
    {
        fichier.cpteur++;
        this->m_tampon.push_back(fichier);          //on utilise la meme structure de données pour toute les heuristiques
    }
    else    // si le tampon est full
    {       // on insère en fonction de l'heuristique
            // 0 : par défaut : Tampon = FiFo
        if(this->getHrstq() == 0 )
        {
            fichier_t temp = this->m_tampon.front();
            if(temp.flag) {                         // if dirty(true) on l'ecrit sur disque dure
                this->writeFile(file);
            }
            this->m_tampon.pop_front();             //ensuite on enlève le fichier de la liste
            fichier.cpteur++;
            this->m_tampon.push_back(fichier);      //on insère le nouvel élément qu'on vient de lire
        }
        else // 1 == LRU(en fonction du compteur)
        {
            this->m_tampon.sort();                  // on trie la liste en fonction du compteur
            fichier_t temp = this->m_tampon.front();
            if(temp.flag){                          // if dirty(true) on l'ecrit sur disque dure
                this->writeFile(file);              // on effectue les opérations d'écriture sur disque
            }
            this->m_tampon.pop_front();             // ensuite on enlève le fichier de la liste
            fichier.cpteur++;                       // on garde chaque action sur le fichier
            this->m_tampon.push_back(fichier);      // on insère le nouvel élément qu'on vient de lire
        }
    }
    // pour chaque fichier n'étant pas présent dans le buffer au moment de la lecture
    // en les chargeant; il y a un coup d'access de 12 ms
    if(this->m_adress->dejaAcceder()){
        this->m_adress->calculTempsAcces(file);
    }
    else
    {
        this->m_adress->addToTemps(12);
        this->m_adress->marquerAcces(true);
        this->m_adress->setAdressePrecedente(file);
    }
    
}
/*
 *  @name  : showbuffer()
 *  @param : 0
 *  @return: void
 *  @desc  : affiche le contenu du buffer en fonction de l'heuristique
*/
void Tampon::showBuffer()
{
    if(!this->m_tampon.empty())
    {
        if(this->getHrstq() == 0)//fifo
        {
            this->buffer();
        }
        else // on affiche en fonction des moins souvent utilisés
        {
            this->m_tampon.sort();
            this->buffer();
        }
    }
    else
    {
        cout << "Le bassin est vide" << endl;
    }
}
void Tampon::buffer()
{

    cout << "fichier n (bit sale) " << endl;
    for(it = this->m_tampon.begin();
        it != this->m_tampon.end(); it++)
    {
        cout << "| " << (*it).elt  << "("
                     << (*it).flag << ")" ;
    }
        cout << "| " << endl;
}
/*
 *  @name  : writeFile
 *  @param : l'adresse du fichier sur le disque dur
 *  @return: void
 *  @desc  : écrit le fichier sur le disque dur
*/
void Tampon::writeFile(short file)
{
    this->m_adress->calculTempsAcces(file);
}
/*
 *  @name  : closeFile
 *  @param : l'adresse du fichier sur le disque dur
 *  @return: void
 *  @desc  : force l'écriture du fichier sur le disque dur
*/
void Tampon::closeFile(short file)
{
    this->writeFile(file);                  // on écrit le fichier sur le disque dure
    for(it = this->m_tampon.begin();
        it != this->m_tampon.end(); it++)  //on parcourt le buffer
    {
        if((*it).elt == file)
        {
            this->m_tampon.erase(it);      // enleve le fichier du buffer
            break;                          // on quitte la boucle
        }
    }
}
/*
 *  @name  : closeBuffer
 *  @param : void
 *  @return: void
 *  @desc  : force l'écriture de tous les fichiers sur le disque dure
 *           dont les bit sont dirty
*/
void Tampon::closeBuffer()
{
    for(it = this->m_tampon.begin();
        it != this->m_tampon.end(); it++)
    {
        if((*it).flag)
        {
            (*it).flag = false;
            this->writeFile((*it).elt); 
        }
    }
    this->m_adress->marquerAcces(false);
    this->buffer();
    this->m_tampon.clear();//on vide le buffer
}
/*
 *  @name  : modifyFile
 *  @param : adresse du fichier
 *  @return: void
 *  @desc  : lit le fichier dans le buffer et set le flag dirty
*/
void Tampon::modifyFile(short file)
{
    this->readFile(file);
    this->setFlagOnblock(file);
}
/*
 *  @name  : getTempTotal
 *  @param : void
 *  @return: int : temps écoulé en ms
 *  @desc  : retourne le temps total, calculé durant toutes les opérations
*/
int Tampon::getTempsTotal(void)
{
    return this->m_adress->getTempsTotal();
}
/*
 *  @name  : resetTempsTotal
 *  @param : void
 *  @return: void
 *  @desc  : reinitialise le temps à zéro
*/
void Tampon::resetTempsTotal(void)
{
    this->m_adress->resetTempsTotal();
}